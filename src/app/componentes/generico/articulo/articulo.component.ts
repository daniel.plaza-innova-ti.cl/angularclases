import { Component, OnInit, Input } from '@angular/core';
import { Articulo } from 'src/app/pojo/articulo';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css']
})
export class ArticuloComponent implements OnInit {

  @Input() public articulo: Articulo;

  constructor() { }

  ngOnInit() {
  }

}
