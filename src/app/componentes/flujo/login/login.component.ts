import { Component, OnInit } from '@angular/core';
import { ClienteWsService } from 'src/app/cliente-ws.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private _clienteWs: ClienteWsService
  ) { }

  ngOnInit() {
    this._clienteWs.obtenerUsuarios().subscribe(
      (exito: any) => {
        console.log('exito');
      },
      (error) => {
        console.log(error);
      },
      () => {
        console.log('Se terminó la ejecución');
      }
    );
  }

}
