import { Component, OnInit } from '@angular/core';
import { Articulo } from 'src/app/pojo/articulo';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  public listaArticulos: Array<Articulo> = new Array<Articulo>();

  constructor() {
   }

  ngOnInit() {
    this.listaArticulos.push(new Articulo('Hola estoy aprendiendo', 'Estoy aprendiendo angluar'));
    this.listaArticulos.push(new Articulo('Hola estoy olvidando', 'Estoy olvidando angluar'));
    console.log(this.listaArticulos);
  }

}
