import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteWsService {

  constructor(
    private _http: HttpClient
  ) { }

  public obtenerUsuarios(): Observable<any> {
    const obs: Observable<any> = this._http.get('http://127.0.0.1:8000/api/getUsuarios');
    return obs;
  }
}
