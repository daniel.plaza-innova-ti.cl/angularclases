import { TestBed, inject } from '@angular/core/testing';

import { ClienteWsService } from './cliente-ws.service';

describe('ClienteWsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClienteWsService]
    });
  });

  it('should be created', inject([ClienteWsService], (service: ClienteWsService) => {
    expect(service).toBeTruthy();
  }));
});
