import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InicioComponent } from './componentes/flujo/inicio/inicio.component';
import { AppRoutingModule } from './/app-routing.module';
import { HeaderComponent } from './componentes/generico/header/header.component';
import { FooterComponent } from './componentes/generico/footer/footer.component';
import { LoginComponent } from './componentes/flujo/login/login.component';
import { ArticuloComponent } from './componentes/generico/articulo/articulo.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ArticuloComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
